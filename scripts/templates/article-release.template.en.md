We have just published a bugfix release for Solarus, which is now at version **%(version)s**.

## Changelog

Here is the exhaustive list of all changes brought by Solarus %(version)s. Thanks to all contributors.

%(content)s
