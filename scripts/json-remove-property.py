# Simple Python script to walk JSON files recursively and remove a property to
# every one.
# Example:
# python scripts/json-remove-property.py -root "fr/entities/article/old/2017" -name "level1/level2/meta_image" -v -r

import glob
import json
import os
import argparse

import jsonutils

if __name__ == '__main__':
  # Create parameter parser
  parser = argparse.ArgumentParser(description="Simple script to remove a property to JSON files.")
  parser.add_argument("-root", type=str, help="Folder to process")
  parser.add_argument("-name", type=str, help="Property name to remove")
  parser.add_argument("-v", "--verbose", help="Increase output verbosity", action="store_true")
  parser.add_argument("-r", "--recursive", help="If the subfolders must also be processed", action="store_true")

  # Parse parameters
  args = parser.parse_args()
  
  # Print parameters if verbose
  if args.verbose:
    print("Verbose mode enabled.\n")
    print("Root:               " + str(args.root))
    print("Recursive:          " + str(args.recursive))
    print("Property to remove: " + str(args.name))
  
  def process_callback(file_path, property_name):
    return jsonutils.remove_property_to_file(file_path, property_name)
  
  jsonutils.process_all_files(args.root, args.name, process_callback, recursive=args.recursive, verbosity=args.verbose)
