Solarus is officially available for Linux in the **Snap Store**:

[![Snapstore](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/solarus)

Alternatively, you can click on the following button to download the Solarus **snap package file** directly.
