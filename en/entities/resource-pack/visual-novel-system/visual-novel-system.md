### Presentation

The *Visual Novel System* is an overhaul of the Solarus dialog system. It allows you to quicky and easily customize every facet of your dialogs.

- Customize the boxes, the names, the characters, backgrounds, and even the SFXs played!
- Override your configs on a per character, per scene, or even per language basis.
- Change your text anyway you choose, from size, to color, font, etc.... even in the middle of a line!
- Insert images and even animated sprites in the middle of your text lines.
