# {title}

[youtube id="rCGwzmdiKk8"]

## Summary

- Working with destructible object entities
  - Bush (can cut & lift)
  - Stone (can lift)
  - Vase
  - Bomb flowers (explode & regenerate) *(mentioned)*
  - Grass (can cut and traversable)
- Working with lift ability (and weight of destructible objects)
  - Showing a dialog when player cannot lift object *(mentioned)*
- Setting damage property for lifted objects

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
