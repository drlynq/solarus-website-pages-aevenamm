# {title}

[youtube id="IFS3eeEVHWw"]

## Summary

- Working with switch entities
  - Walkable switch
  - Solid switch
- Using a switch from a map script
  - Make a chest appear (and save state)
- Events used by a switch
  - The `on_activated()` event
  - The `on_deactivated()` event

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
