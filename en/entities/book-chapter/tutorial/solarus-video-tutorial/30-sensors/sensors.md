# {title}

[youtube id="XJEEdqijh60"]

## Summary

- Working with sensor entities
  - Changing the hero's position when a sensor is activated
  - Moving hero to room entrance after falling in hole (and save solid ground position)
- Differences between sensors and switches/teletransporters

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
