# {title}

[youtube id="__Ct85wNISo"]

## Summary

- Working with separator entities
  - Individual rooms with separators
  - Adding separator in middle of big room
  - Big room with separator removed
  - How to fix scrolling problem with doors when too close to T separators
- Separator events
  - The `on_activating()` event
  - The `on_activated()` event

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
