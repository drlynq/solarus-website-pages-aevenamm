# {title}

[youtube id="mYjctnDqsrU"]

## Summary

- Working with door entities
  - Opening doors when switch activated
  - Opening doors when hero interacts with them (and save state to savegame value)

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
