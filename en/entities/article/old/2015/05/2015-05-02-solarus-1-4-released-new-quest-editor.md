A new version of Solarus and Solarus Quest Editor is available: version 1.4!

We have been working hard for the last few months to completely rewrite the quest editor. The new quest editor is now written in C++/Qt. It is faster, nicer and has a lot of new features!

<a href="http://www.solarus-games.org/wp-content/uploads/2015/05/tileset-editor.png"><img class="aligncenter size-medium wp-image-998" src="/images/tileset-editor-300x181.png" alt="tileset-editor" width="300" height="181" /></a>
<ul>
	<li>Dialogs editor!</li>
	<li>Strings editor!</li>
	<li>Quest properties editor!</li>
	<li>Options dialog!</li>
	<li>Ability to run the quest from the editor! (experimental)</li>
	<li>Multi-selection support in the tileset view!</li>
	<li>Improved quest tree!</li>
	<li>French translation!</li>
</ul>
And a lot more, see the full changelog below.

<a href="http://www.solarus-games.org/wp-content/uploads/2015/05/dialog-editor.png"><img class="aligncenter size-medium wp-image-999" src="/images/dialog-editor-300x181.png" alt="dialog-editor" width="300" height="181" /></a>

There might be some bugs we missed in the new editor since everything was rewritten from scratch. Please report any issue on the <a href="https://github.com/christopho/solarus-quest-editor/issues/">bug tracker</a>.

There are not a lot of changes in the engine (mostly bug fixes). The only real change is that fonts are now a resource like maps, tilesets, musics, sprites, etc. Your scripts may need slight adjustements to follow this change. Nothing dangerous, don't worry, but this is explained in the <a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">migration guide</a>.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/download/">Download Solarus 1.4</a></li>
	<li><a title="Solarus Quest Editor" href="http://www.solarus-games.org/engine/solarus-quest-editor/">Solarus Quest Editor</a></li>
	<li><a title="LUA API documentation" href="http://www.solarus-games.org/doc/latest/lua_api.html">Lua API documentation</a></li>
	<li><a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">Migration guide</a></li>
</ul>
We hope you will enjoy the new quest editor! Check our <a href="http://www.solarus-games.org/development/tutorials/">tutorials</a> if you want to learn how to create your own quest. We are also currently updating the website to reflect the new change and give up-to-date information and screenshots of the quest editor.

As always, our games ZSDX and ZSXD were also upgraded to give you up-to-date examples of games.
<ul>
	<li><a title="Download ZSDX" href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/">Download Zelda Mystery of Solarus DX 1.10</a></li>
	<li><a title="Download ZSXD" href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/">Download Zelda Mystery of Solarus XD 1.10</a></li>
</ul>
Here is the full changelog.
<h2>Changes in Solarus 1.4</h2>
New features:
<ul>
	<li>Solarus now compiles with C++11.</li>
	<li>Solarus can now be used as a library in other projects.</li>
	<li>Add a command-line flag -win-console=yes to see output on Windows (#550).</li>
	<li>Add unit tests.</li>
	<li>fonts.dat no longer exists. Fonts are a resource like others now (#611).</li>
	<li>Fonts are now in a "fonts" directory instead of "text".</li>
	<li>Maps: shop treasures have a new property "font".</li>
</ul>
Lua API changes that introduce incompatibilities:
<ul>
	<li>Text surfaces: the size must now be set at runtime instead of in fonts.dat.</li>
	<li>Text surfaces: the default font is now the first one in alphabetical order.</li>
</ul>
Lua API changes that do not introduce incompatibilities:
<ul>
	<li>sol.text_surface.create() now accepts a size parameter (default is 11).</li>
	<li>Add a function sol.main.get_os().</li>
	<li>Fix sprite:on_frame_changed() called twice on animation/direction change.</li>
</ul>
Bug fixes:
<ul>
	<li>Add mouse functions and events (experimental, more will come in Solarus 1.4).</li>
	<li>Add a method sprite:get_animation_set_id() (#552).</li>
	<li>Add a method sprite:has_animation() (#525).</li>
	<li>Add a method sprite:get_num_directions().</li>
	<li>Add a method hero:get_solid_ground_position() (#572).</li>
	<li>Add a method switch:get_sprite().</li>
	<li>Allow to <strong>customize the sprite and sound of switches</strong> (#553).</li>
	<li>Add a method enemy:get_treasure() (#501).</li>
</ul>
<h2>Changes in Solarus Quest Editor 1.4</h2>
New features:
<ul>
	<li>Solarus Quest Editor was rewritten and is now in a separate repository.</li>
	<li>Dialogs editor (by Maxs).</li>
	<li>Strings editor (by Maxs).</li>
	<li>Quest properties (quest.dat) editor (by Maxs).</li>
	<li>French translation.</li>
	<li>Add an options dialogs to configure various settings (by Maxs).</li>
	<li>Add a toolbar.</li>
	<li>The quest can now be run from the editor (experimental).</li>
	<li>New keyboard shortcuts.</li>
	<li>Map editor: multiple tiles from the tileset can now be added at once.</li>
	<li>Map editor: improved the resizing of multiple entities.</li>
	<li>Map editor: show information about the entity under the cursor.</li>
	<li>Map editor: multiple tiles can be converted to/from dynamic tiles at once.</li>
	<li>Map editor: tiles whose pattern is missing are no longer removed.</li>
	<li>Tileset editor: undo/redo support.</li>
	<li>Tileset editor: multi-selection support.</li>
	<li>Tileset editor: showing the grid is now supported.</li>
	<li>Sprite editor: undo/redo support.</li>
	<li>Sprite editor: showing the grid is now supported.</li>
	<li>Sprite lists now show icons representing each sprite.</li>
	<li>Code editor: add a find text feature.</li>
	<li>Quest tree: organize the view in columns (file name, description, type).</li>
	<li>Quest tree: show resources whose file is missing on the filesystem.</li>
	<li>Quest tree: show files that look like resources but are not declared yet.</li>
	<li>The quest tree now automatically refreshes after changes in the filesystem.</li>
	<li>Resource selectors now show a hierarchical view of elements.</li>
	<li>A file to open can now be passed on the command line.</li>
	<li>Show an appropriate icon in tabs.</li>
	<li>Show an asterisk in the tab of files that are changed.</li>
	<li>The editor has an icon now.</li>
	<li>Add a menu item to open the official Solarus website.</li>
	<li>Add a menu item to open Solarus documentation.</li>
</ul>
Bug fixes:
<ul>
	<li>Fix encoding of non-ascii characters in project_db.dat with Windows.</li>
	<li>Fix Ctrl+S shortcut not always working.</li>
	<li>Fix entities restored to the front when undoing removing entities.</li>
	<li>Fix entities restored to the front when undoing changing their layer.</li>
	<li>Fix separators resizing that could allow illegal sizes.</li>
	<li>Fix jumpers size reset after undoing changing their direction.</li>
	<li>Fix walls being initially traversable by everything when they are created.</li>
</ul>
<h2>Incompatibilities</h2>
These improvements involve changes that introduce a slight incompatibility in the  Lua API regarding fonts. Make a backup, and then see the <a href="http://wiki.solarus-games.org/doku.php?id=migration_guide">migration guide</a> on the wiki to know how to upgrade.
<h2>Changes in Zelda Mystery of Solarus DX 1.10</h2>
<p id="LC4" class="line">Bug fixes:</p>

<ul>
	<li class="line">Fix a minor tile issue in dungeon 8 entrance.</li>
	<li class="line">Fix the feather dialog that could be skipped in some languages.</li>
	<li class="line">Fix empty line in English dialog describing a bottle with water.</li>
	<li class="line">Fix empty buttons in savegame menu when changing language (#90).</li>
	<li class="line">Fix translation errors of two items in German (#98).</li>
	<li class="line">Dungeon 5: fix a missing door on the minimap (#99).</li>
	<li class="line">Dungeon 9: don't allow to skip the boss hint dialog (#93).</li>
</ul>
<h2>Changes in Zelda Mystery of Solarus XD 1.10</h2>
<p id="LC7" class="line">Bug fixes:</p>

<ul>
	<li class="line">Fix fairy only restoring 1/4 heart in cursed cave (#40).</li>
</ul>