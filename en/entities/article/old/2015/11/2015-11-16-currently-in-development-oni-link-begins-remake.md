<a href="http://www.solarus-games.org/wp-content/uploads/2015/09/olb_se_logo.png"><img class="aligncenter wp-image-1322 size-large" src="/images/olb_se_logo-1024x700.png" alt="olb_se_logo" width="720" height="492" /></a>

Christopho and Mymy have begun to work on <em>Oni Link Begins</em>, the sequel to <em>Return of the Hylian</em>. This is also an original game by Vincent Jouillat, and the second part of his trilogy. There is no release date announced, but you can watch the progression of the remake.
<ul>
	<li>Github: <a href="https://github.com/christopho/zelda_olb_se">https://github.com/christopho/zelda_olb_se</a></li>
	<li>Mapping videos: <a href="https://www.youtube.com/watch?v=d9_E6c7lYOY">part 1</a>, <a href="https://www.youtube.com/watch?v=SvwLzam1jVA">part 2</a></li>
</ul>