I have finished another new feature for Solarus 1.1: you can now separate a map in different regions. When you are in a region, you cannot see adjacent regions. And when you touch the limit of the region, the camera scrolls to the region on the other side.

You can use this feature to visually separate rooms that are in the same map. In ZSDX, when you are in a room of a dungeon, you can also see parts of the adjacent rooms. If you want to only show the current map (like in Zelda ALTTP), it is possible now. With the quest editor, you create a separator, which is a vertical or horizontal bar that you place on your map.

Separators implictly define regions on your map. Thus, you can also use this feature to make a minimap that shows the visited rooms of the dungeon.