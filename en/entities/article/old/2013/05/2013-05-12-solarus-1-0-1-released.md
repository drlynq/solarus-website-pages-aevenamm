&nbsp;

An update of Solarus 1.0 is available!

This update fixes a bunch of bugs of previous versions (some of them were very old!), and improves the documentation as well as the quest editor. It is recommended to upgrade.

Because it is a patch release (1.0.0 to 1.0.1), there is no change in the format of data files or in the Lua API, everything remains compatible.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/downloads/download-solarus/">Download Solarus 1.0.1</a></li>
	<li><a title="Solarus Quest Editor" href="http://www.solarus-games.org/solarus/quest-editor/">Solarus Quest Editor</a></li>
	<li><a href="http://www.solarus-games.org/doc/1.0.0/lua_api.html">Lua API documentation</a></li>
</ul>
<h2>Changes in Solarus</h2>
<ul>
	<li>Fix the Mac OS X port.</li>
	<li>Fix jump movement accross two maps ending up in a wall (#189).</li>
	<li>Fix a possible crash in TextSurface.</li>
	<li>Fix the hero disappearing a short time after using the sword (#35).</li>
	<li>Fix the boomerang failing to bring back pickables sometimes (#187).</li>
	<li>Fix parallax scrolling tiles not always displayed (#167).</li>
	<li>Fix the setting joypad_enabled that had no effect (#163).</li>
	<li>Fix doors not working when they require equipment items.</li>
	<li>Fix a possible compilation warning in Surface.cpp.</li>
	<li>Fix creating a transition from the callback of a previous one.</li>
	<li>Fix crystal blocks animated late when coming from a teletransporter (#61).</li>
	<li>Fix arrows that got stopped when outside the screen (#73).</li>
	<li>Fix diagonal movement that failed in narrow passages (#39).</li>
	<li>Don't die if a script makes an error with a sprite (#151).</li>
	<li>Don't die if a script makes an error with an enemy attack consequence.</li>
	<li>Allow enemies to lose 0 life points when attacked (#137).</li>
	<li>Pixel-precise collisions can now also be performed on 32-bit images.</li>
</ul>
<h2>Changes in Solarus Quest Editor</h2>
<ul>
	<li>Add the possibility to show or hide each type of entity (#60).</li>
	<li>Keep the map coordinates shown when changing the zoom (#183).</li>
	<li>Fix the map view not updated correctly when changing the zoom (#174).</li>
	<li>Show the correct sprite of destructible objects (#77).</li>
	<li>Show an appropriate error message if the LuaJ jar is missing (#173).</li>
	<li>Fix the title bar string (#176).</li>
</ul>
<h2>Documentation changes</h2>
<ul>
	<li>Split the <a href="http://www.solarus-games.org/developer_doc/latest/">C++ documentation</a> and the <a href="http://www.solarus-games.org/doc/latest/quest.html">quest data files documentation</a> (#181).</li>
	<li>Add a search feature to the documentation pages.</li>
</ul>
Enjoy!