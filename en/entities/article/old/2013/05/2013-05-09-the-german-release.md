As promised in my previous post, I have just released new versions of <a title="Download ZSDX" href="http://www.solarus-games.org/downloads/download-zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus DX</a>, <a title="Download ZSXD" href="http://www.solarus-games.org/downloads/download-zelda-mystery-of-solarus-xd/">Zelda Mystery of Solarus XD</a> and Solarus!

These updates still use the old 0.9 branch of Solarus because I haven't finished to convert both games yet to Solarus 1.0. And you have waited long enough for the German release and for important bug fixes, especially the bug of blocks that only made half moves.

Solarus 0.9.3 changes:
<ul>
	<li>The game screen size can now be set at compilation time.</li>
	<li>Change the savegames directory on Mac OS X.</li>
	<li>Improve the support of Mac OS X, Android, Pandora, Caanoo and other platforms.</li>
	<li>Fix the compilation with Visual C++.</li>
	<li><strong>Fix blocks making sometimes only a half move (#33).</strong></li>
	<li>Fix pixel-precise collisions not always correct (#53).</li>
	<li>Fix the end of target movement on slow machines (#34).</li>
	<li>Fix the hero being freezed when using the hookshot on bomb flowers (#119).</li>
</ul>
This should be the last release in the Solarus 0.9 branch. Our games are currently being converted to the 1.0 format.

ZSDX 1.5.2 changes:
<ul>
	<li><strong>German dialogs available.</strong></li>
	<li>Castle 1F: fix NPCs possibly overlapping the hero.</li>
	<li>West mountains cave: fix a chest possibly overlapping the hero.</li>
	<li>Fix various issues in French, English and Spanish dialogs.</li>
	<li>Fix the CMake script that creates the zip archive (#1).</li>
</ul>
ZSXD 1.5.3 changes:
<ul>
	<li>Dungeon 2: the piece of heart could be obtained very easily with the sword.</li>
	<li>Change the English website url in the credits dialogs.</li>
	<li>Fix missing lines in dialogs.</li>
	<li>Fix the CMake script that creates the zip archive.</li>
	<li>Make creepers appear earlier in the game (#1). Because creepers are fun. I like creepers.</li>
</ul>
&nbsp;