Welcome to the development blog of Solarus, the open-source Zelda-like 2D game engine. Solarus is the engine developed for the game Zelda: Mystery of Solarus DX. This blog has several purposes:
<ul>
	<li>Provide access to the source code of the engine (Solarus) and the game(s), including the development version</li>
	<li>Give news about the development and some technical information about how the engine works</li>
	<li>Encourage contributions for translations, ports to new systems, development tools, and new quests</li>
</ul>
Some pages are already available and others will come soon. For now, you can get the <a href="http://www.solarus-engine.org/source-code/">source code</a> and see the <a href="http://www.solarus-engine.org/zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus DX</a> section. You can also <a href="http://www.solarus-engine.org/contact/">contact me</a> for any suggestion.