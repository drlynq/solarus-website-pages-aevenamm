<strong>Who said it was too late? It's never too late!</strong>

[embed]https://www.youtube.com/watch?v=M2tAk5hRMHk[/embed]

Exactly <strong>one year after the original release</strong> of the sequel to <a href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/">our first parodic game</a>, we are proud to finally offer an to the English speakers the opportunity to play our game in an <strong>all-English version</strong>. The translation wasn't easy : the original French release contains multiple references to French-speaking culture (Comic groups Les Nuls, Les Inconnus, cult movies like <em>Cit� de la peur</em> and <em>OSS 117 � Rio</em>, and of course the beloved cult TV show <em>Kaamelott</em>).

As a <strong>direct sequel</strong> (the story begins only months after the first episode), the overworld is expanded, the story is extended and the funny tone is kept, if not bettered!
<p style="text-align: center;"><a href="http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/"><img class="aligncenter wp-image-1632 size-medium" src="/images/box_zelda_xd2-300x214.png" alt="" width="300" height="214" /></a><a href="http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/">Click here to download the game</a></p>
The game is longer than the first episode: it has <b>2 dungeons</b>, and <b>lots of side-quests</b> and secrets everywhere, and also features silly NPCs all around the overworld. Be sure to speak to every one to not miss a joke! The estimated game duration is <b>7-12h of playing</b>.
<p style="text-align: center;"></p>
We hope that you will enjoy our little game. Have fun!