Max Mraz, who is also working on his much bigger game [Ocean's Heart](/en/games/oceans-heart), has just released a short game called **Yarntown**. It is a homage to the already legendary game Bloodborne, originally released for PS4 in 2015. Don't be fooled by the cuteness of the graphics, the game is brutally hard and will be merciless, just like its inspiration!

- Download the game on [its page in the Solarus Quest Library](/en/games/yarntown).
- Download the game on [itch.io](https://maxatrillionator.itch.io/yarntown)

![Screenshot 1](images/screen_1.png)

![Screenshot 2](images/screen_2.png)
