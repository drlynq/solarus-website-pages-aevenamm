Apart from the engine (still GPL), we made games and Lua code that we licensed with **GPL**. However, GPL is not compatible with closed-source projects, so the Lua scripts were not allowed to be used in commercial projects. The community then started [MIT Starter Quest](/en/development/resource-packs/mit-starter-quest), a MIT-compatible resource pack that allows you to make a commercial Solarus quest.

**The Solarus logo was previously licensed under GPL (code) and CC-BY-SA (sprite, sound)**, which meant that using it in a non-GPL project would make the project also GPL, which was unwanted. So, in agreement with its creators (Olivier Cléro for the art, Maxs for the code, Diarandor for the sound), we decided to change its license.

**It is now licensed under MIT (code) and CC-BY (sprite, sound)**, which means you can now use it in a closed-source commercial project, as long as you credit the authors. It'll be included in the MIT Starter Pack.

It affects the files:

- `solarus_logo.lua`
- `solarus_logo.png`
- `solarus_logo.ogg`

If licensing is still a question, you may find help on the [tutorial chapter about licensing](/en/development/tutorials/solarus-official-guide/basics/choosing-a-license).
