[container]

# {title}

## History

Everything began back in 2002, when Christopho, the creator and main developer of Solarus, created a website for its first fangame project: **Zelda-Solarus**. It was a way to promote the first version of **Mystery of Solarus**, a game developed with RPG Maker and designed as a follow-up to *The Legend of Zelda: A Link to the Past*.

As time passing by, the website and its forum grew, and began to be a place not only for French Zelda-like game makers, but also Zelda-lovers since it was a general Zelda website as well with mangas, walkthroughs, reviews, resources and news.

In 2006, Christopho decided to rework his first game in C++, which then became [Mystery of Solarys DX](/en/games/the-legend-of-zelda-mystery-of-solarus-dx), and **Solarus** was born.

Later, it was decided that the engine needed a website for itself. Both websites eventually merged in 2019, to become the **Solarus-Games website** you know today.

The original core team, plus new members, kept on making games with the engine, under the name **Solarus Team**, independently of working on the engine.

The project continues to grow up and is becoming a general Action-RPG game engine. More people are joining the project, and we are receiving more and more contributions. To such an extent that a nonprofit organization named [Solarus Labs](/en/about/nonprofit-organization) was created to promote the project and handle donations better.

## People

### Main active contributors

Solarus is developed by a community of voluntary enthusiasts who contribute code, bug reports, documentation, artwork, support, etc. on their free time, as a hobby. Use this list if you need some help or guidance in any of the project fields.

* [Christopho](https://twitter.com/ChristophoZS) (Founder, Lead Developer)
* [Std::Gregwar](https://gitlab.com/stdgregwar) (Co-lead Developer)
* [Hugo Hromic](https://gitlab.com/hhromic) (DevOps)
* [Olivier Cléro](https://twitter.com/olivclr) (Artwork, Communication)
* [Binbin](https://twitter.com/zelda_force) (Web Developer)

### All contributors

It is impossible to list all the contributors. Nevertheless, this list displays, in alphabetical order, people who contributed significantly to the project (over 10 commits). Thanks to all of them, they helped making Solarus what it is today!

* 19oj19
* Alex Gleason
* BenObiWan
* Binbin
* Clem04
* Cluedrew Kenfar Ink
* Christopho
* Diarandor
* Hugo Hromic
* Max Mraz
* Maxs
* Metallizer (Co-founder)
* Michel Hermier
* Morwenn
* Mymy
* Nathan Moore
* Newlink
* Olivier Cléro
* Perry Thompson
* Peter De Wachter
* PhoenixII54
* Renkineko
* Samuel Lepetit
* Sergio C
* Shin-NiL
* Std::gregwar
* Vlag
* Xethm55
* Zane Kukta
* 高彬彬

### Donators

* Alex Gleason (100€)
* Adrian Moga (50€)
* Diegopau (34€)
* 6S-Studio (25€)
* David Wisser (20€)
* EthanGold60 (5€)

[/container]
