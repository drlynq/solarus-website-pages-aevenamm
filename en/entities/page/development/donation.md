[container layout="small"]

# {title}

## Why donate

All the software made in the context of the Solarus engine project is [free and open-source](https://gitlab.com/solarus-games), and will be forever. It includes the engine itself, but also the tools for players and developers, all the games made by Solarus Team, the website engine, etc. We spend a lot of time working on the project, and we intend to continue!

Here is, for instance, how the donations will be used:

* Hiring developers or artists to work on the project,
* Paying the server hosting fees,
* Purchasing required stuff to develop Solarus (hardware, software),
* Covering travel costs for meetings, conferences and other events,
* Producing Solarus merchandising (autocollants, t-shirts, posters, etc.).

Money will go to [Solarus Labs](/en/about/nonprofit-organization), the nonprofit organization that promotes Solarus, and will be totally reinvested into the project.

![Solarus Labs logo](images/solarus-labs-logo-small.png)

You can donate any amount you want via the following ways. Please [contact us](/en/about/contact) if you want to donate via a different way. Your support is very appreciated!

The list of donators is feature [here](/en/about/contributors) on the website.

## How to donate

[paypal title="Paypal" email="solarus-labs@solarus-games.org" name="Solarus Labs donation" label="Donate" icon="paypal" icon-category="fab" subject="Solarus"]

Use Paypal to donate to Solarus Labs. You don't need to have a paypal account in order to make a donation.

[/paypal]

[donation title="Liberapay" label="Donate" icon="donate" link="https://liberapay.com/solarus-labs/donate"]

Use Liberapay to donate to Solarus Labs on a regular basis. Liberapay is a nonprofit organization that manage a recurrent donations platform.

[/donation]

[bitcoin title="Bitcoin" image="/data/assets/images/bitcoin_qrcode.png" icon="bitcoin" icon-category="fab" label="Donate" link="bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg"]

Use the following address or the following QR-Code:
[align type="center"]

#### [`1DyA88zEr1P2phrMWm76QNxc82RRC3nR95`](bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg)

[![Bitcoin QR Code](../../../../assets/images/bitcoin_qrcode.png)](bitcoin:1DyA88zEr1P2phrMWm76QNxc82RRC3nR95)

[/align]

[/bitcoin]

[/container]
