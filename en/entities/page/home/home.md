<!--Header-->
[container layout="full"]
[cover is-parallax="true"]
[row]
[column width="7"]
[jumbotron title="A lightweight, free and open-source game engine for Action-RPGs"]
[hr width="60"]

* A 2D game engine written in C++, and executing games made in Lua.
* Specifically designed with 16-bit classic Action-RPGs in mind.
* Available on multiple platforms.
* Completely free and open-source, under GPL v3 License.

[space]

[button type="primary" outline="true" label="Learn more" url="/en/solarus/overview"]
[space orientation="vertical" thickness="20"]
[button type="primary" label="Download" url="/en/solarus/download"]
[space]

[icon icon="windows" category="fab" size="2x"]
[icon icon="apple" category="fab" size="2x"]
[icon icon="linux" category="fab" size="2x"]
[icon icon="freebsd" category="fab" size="2x"]
[icon icon="android" category="fab" size="2x"]
[icon icon="raspberry-pi" category="fab" size="2x"]

[space thickness="10"]

[small]Stable version 1.6.5 • Updated on April 6th, 2021[/small]

[space thickness="40"]

[/jumbotron]
[/column]

[column width="5"]
[align type="left"]
![Illustration](images/illustration.svg)
[/align]
[/column]
[/row]

[/cover]
[/container]

<!--Games-->
[highlight]
[space]
[container]

# Games

[game-featured id="game-yarntown" title="Featured game" button-label="More info"]

## Available

[row]
[column]
[thumbnail id="game-the-legend-of-zelda-xd2-mercuris-chess"]
[/column]
[column]
[thumbnail id="game-defi-de-zeldo-ch-2"]
[/column]
[column]
[thumbnail id="game-the-legend-of-zelda-mystery-of-solarus-dx"]
[/column]
[column]
[thumbnail id="game-tunics"]
[/column]
[/row]
[space thickness="15"]

## Upcoming

[row]
[column]
[thumbnail id="game-vegan-on-a-desert-island]
[/column]
[column]
[thumbnail id="game-oceans-heart"]
[/column]
[column]
[thumbnail id="game-the-legend-of-zelda-a-link-to-the-dream]
[/column]
[column]
[thumbnail id="game-the-legend-of-zelda-onilink-begins-se"]
[/column]
[/row]
[space thickness="60"]

[container_1 layout="small"]
[row]
[column width="3"]
[/column]
[column width="6"]
[button-highlight type="primary" icon="bag" url="/en/games" label="Solarus Quest Library" subtitle="More games in"]
[/column]
[/row]
[/container_1]

[space thickness="20"]

[/container]
[/highlight]

<!--Getting Started-->
[container]
[space thickness="0"]

# Getting started

If you want to create a game with Solarus, you will find help to make the learning curve less steep.

[space thickness="20"]

[container_1 layout="small"]
[row]
[column]

[button-highlight type="primary" icon="owl" url="/en/development/tutorials" label="Tutorials"]

[/column]
[column]

[button-highlight type="primary" icon="book" url="https://www.solarus-games.org/doc/latest" label="Documentation"]

[/column]
[column]

[button-highlight type="primary" icon="talk" url="https://forum.solarus-games.org/" label="Forums"]

[/column]
[/row]
[/container_1]
[/container]

[space]

[container]

# Latest news

[article-listing read-more-label="Read more..." column-width="4" count="3"]

[container_1 layout="small"]
[row]
[column width="3"]
[/column]
[column width="6"]
[button-highlight type="primary" icon="calendar" url="/en/news" label="Solarus Development Blog" subtitle="More articles on"]
[/column]
[/row]
[/container_1]

[/container]
