# Adding a game to the library

## Required elements

To add your game to the library, you must provide a few files:

* **An icon:** 4 PNG files in 16x16 pixels, 32x32, 64x64, 128x128
* **A cover picture:**, without logo, 1920x400 pixels, in JPG.
* **A logo:** in 480x285 pixels, PNG with transparency
* **A thumbnail:**, 700x360 pixels, PNG
* At least **4 screenshots** (or any multiple of 4), that showcase different parts of the game.
* A **markdown file** where you can write anything you want about the game: presentation, synopsis, etc. NB: top titles must be h3 titles.
* Optional: some additionnal pictures (artworks, etc.) that you can include in your markdown.
* Optional: the logo of the author/studio in PNG (max height 48px.).

The logo will be added above the cover on the game's page. Don't add the logo on the cover. Keep it as a separate file. Avoid important elements on the sides and in the middle, since it will be respectively croppped or hidden by the logo.

![Cover](images/game_template_cover.png)

On the contrary, you must add your logo directly on the thumbnail. Avoid placing important elements along the edges of the thumbnail, since we perform a little zoom when the mouse is over the thumbnail.

![Thumbnail](images/game_template_thumbnail.png)

## Adding your game

### Create the folder structure

First, clone the repository on your computer. Then, don't stay on the default branch, named `dev`. Instead, create a branch that you name with your game's title.

Create a folder with your game's title in `en/entities/game/`, so the path is like this:
`en/entities/game/my-game-title`.
In this folder, create this:

* An `/artworks` folder, if you have some (optional).
* An `/icons` folder. Place here your icons, named like this `icon_WxH.png` where `W`is the width and `H` is the height of the picture. Exemple: `icon_16x16.png` for the smallest icon.
* An `/images` folder where you place `cover.jpg`, `thumbnail.png` and `logo.png`.
* A `/medias` folder where you place your screenshots. Convention is to name them `screen_n.png` where `n` is the index.

### Add the game info

Create two files in your game's folder, name with your game's title:

* `my-game-title.json`: The game's metadata.
* `my-game-title.md`: The game's description.

#### Game metadata

This is the content of your json file.

```json
{
  "age": "All publics",
  "authors": ["Your name"], // Your name or the name of your game studio
  "downloads": [], // coming soon
  "excerpt": "My game is a super awesome game where you have to save the world.", // A sentence to summarize your game.
  "id": "game-my-game-title", // id of your game (begins always with game-)
  "icons": { // The icons
    "16x16": "icons/icon_16x16.png",
    "32x32": "icons/icon_32x32.png",
    "64x64": "icons/icon_64x64.png",
    "128x128": "icons/icon_128x128.png"
  },
  "image_cover": "images/cover.jpg", // The cover
  "image_logo": "images/logo.png", // The logo
  "image_thumbnail": "images/thumbnail.png", // The thumbnail
  "languages": ["en", "fr", "es"], // The languages available in your game (standardized ISO).
  "licenses": ["GPL v3", "CC-BY-SA 4.0", "Proprietary (Fair use)"], // The licenses used for your game.
  "medias": [ // The screenshots
    {
      "image": "medias/screen_1.png",
      "thumbnail": "medias/screen_1.png",
      "type": "image"
    },
    {
      "image": "medias/screen_2.png",
      "thumbnail": "medias/screen_2.png",
      "type": "image"
    },
    {
      "image": "medias/screen_3.png",
      "thumbnail": "medias/screen_3.png",
      "type": "image"
    },
    {
      "id": "BUxREyXILLs", // For a video, use the Yuotube id.
      "thumbnail": "medias/screen_4.png", // You can use a thumbnail for the video.
      "type": "youtube"
    }
    // You can add as much screenshots as you want, but use multiple of 4 since the gallery display them 4 by 4.
  ],
  "page_override": {
    "meta_description": "My Game Title",
    "meta_title": "My Game Title",
    "slug": "games/my-game-title",
    "title": "My Game Title" // The icons
  },
  "page_slug": "games/my-game-title", // slug of the page.
  "min_players": 1, // Minimum number of players in your game.
  "max_players": 1, // Maximum number of players in your game.
  "publish": true, // true if the page should be visible on the website.
  "release_date": "2017-08-15", // Release date in the form YYYY-MM-DD
  "title": "My Game Title", // Title
  "types": ["Action-RPG", "Adventure"] // The game's genre
} // The thumbnail
```

### Add the game description

This is the content of your md file. Use standard markdown. Images won't be automatically resized so use adapted sizes to be sure to fit in the page. Here is an example of what you can do:

```md
## Description

*My Game* is an awesome adventure in a kingdom far far away. You have to explore **unkown territories** and defeat **evil monsters** to bring peace to the country.

![worldmap](artworks/artwork_worldmap.jpg)

## Synopsis

**Kinl** is a young farmer, living in the countryside of the kingdom. One night, a **beautiful princess** speaks to him by telepathy. She asks for help because the King is dying. Kinl decides to go to the capital to investigate. **Here begins a captivating quest...**

![hero](artworks/artwork_hero.jpg)

```

## Integrate your branch to the website

Push everything to Gitlab, then create a Merge Request in the project's page on Gitlab. Your MR will be examinated and merged if nothing is missing or wrong.
