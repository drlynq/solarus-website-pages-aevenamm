# Contribution

## Notifying of an issue or a missing feature

If you notice an issue (typo, bug, anything) and don't have time to fix it yourself, or if you think a feature is missing, please create an issue in the Gitlab issue tracker.

Before creating a new issue, please check if someone has not already created it before.

* For typos, layout bugs and images issues, please create your issue in [solarus-website-pages' issue tracker](https://gitlab.com/solarus-games/solarus-website-pages/issues).
* For pages or CSS bugs, please create your issue in [kokori's issue tracker](https://gitlab.com/solarus-games/kokori/issues).

If there are multiple typos on the same page, please create only one issue and list them all.

Please provide detailed explanations of the bug, and screenshots if you think it is necessary.

We will happily fix this as soon as possible.

## Writing content

So you want to help us writing content for this website? Thank you very much. Here is how to do it.

> **Note:** As of March 13, 2021, we highly recommend to use the [provided Kokori Docker image](docker-image.md) for developing and testing new website content.

If you have not [installed everything](installation.md), please do it before being able to contribute.

### Create a new branch

As you may know, the branch selected in your Git repository is `dev`. This is the branch where all modifications go, before going public, into the `master` branch, which corresponds to the actual website people will see.

Don't modify the `dev` branch of your fork. Keep it synchronous with the original `dev` branch of the official repository.

So, don't stay on the `dev` branch. Instead, create your own branch. Name it with the feature you will add or modify. Let's say you want to add a page for the game you created with Solarus.

Create a branch:

```bash
git branch my-game-title
```

Then, select it:
```bash
git checkout my-game-title
```

### Work on this branch

Make your modifications in this branch, with clean and small commits please. See Git instructions to know about `git add` and `git commit` commands, if you don't know them.

### Upload your branch

At a certain moment, you'll want to send your modifications online, so that it won't stay only on your computer.

You will then type:

```bash
git push
```

And Git will answer you that, when it's the first time you're pushing commits, you need to actually type this:

```bash
git push --set-upstream origin/my-game-title
```

Voilà! Your modifications are sent online. But wait: currently, they're not yet added to the official repository: they're only on your fork.

### Create a Merge Request

To achieve this, you then need to create a **Merge Request**. Go to the [Merge Requests page](https://gitlab.com/solarus-games/solarus-website-pages/merge_requests) and create yours. Please ensure you selected the official repository as the target branch, i.e. `solarus-games/solarus-website-pages`.

The repository maintainers will examinate your Merge Request and merge it if everything is correct.
