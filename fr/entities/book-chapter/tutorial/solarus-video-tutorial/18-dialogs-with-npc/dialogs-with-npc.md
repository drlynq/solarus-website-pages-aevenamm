# {title}

[youtube id="Qvc2cQzuL-E"]

## Sommaire

- Interagir avec une entité PNJ
- Créer un dialogue de PNJ avec une question
- Créer un script de map pour gérer les interactions avec les PNJ et afficher les dialogues correspondants
- Définir des valeurs dans le fichier de sauvegarde pour se souvenir de l'état des interactions avec les PNJ

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
