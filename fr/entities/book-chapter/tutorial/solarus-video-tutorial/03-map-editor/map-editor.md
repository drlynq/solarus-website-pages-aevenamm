# {title}

[youtube id="6Rt8kXxYgZs"]

## Sommaire

- Utiliser l'éditeur de maps
  - Raccourcis clavier
- Créer et modifier des tiles
  - Tilesets
  - Traversables et types de terrains
  - Couches
  - Redimensionner les tiles

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
