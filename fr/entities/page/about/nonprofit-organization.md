
[container layout="small"]

# {title}

[space thickness="30"]

![Solarus Labs logo](images/solarus-labs-logo.png)

[space thickness="30"]

Le projet Solarus est supporté par l'organisme à but non-lucratif **Solarus Labs**, basé en France.

D'un point de vue légal, l'organisme est une **Association Loi de 1901** selon la loi française. Cette association a pour objet le développement, la promotion et la formation à l'utilisation de solutions informatiques libres pour la création de jeux vidéo.

Tous les dons iront directment à l'association, et seront réinvestis dans le projet.

## Bureau

Le bureau actuel de Solarus Labs est composé de :

* **Président:** Christophe Thiéry
* **Trésorier:** Kévin Baumann
* **Vice-Trésorier:** Benjamin Schweitzer
* **Secrétaire:** Olivier Cléro

[/container]
