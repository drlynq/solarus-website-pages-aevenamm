[container]

# {title}

[space]

[row]

[column]

## Pour les joueurs

![Launcher](images/launcher_miniature.png)

[space thickness="20"]

Ce paquet contient ce que dont avez besoin pour **lancer des quêtes Solarus**, autrement dit des jeux. Si vous souhaitez juste jouer, vous êtes au bon endroit.

[align type="center"]
[button-download type="primary" label="Télécharger" url="#" icon="download" icon-category="fa" icon-size="1x" category="player" modal-title="Télécharger" modal-button-label="Télécharger"]
[/align]

### Contenu du paquet

[box]

[icon-solarus icon="solarus" size="32"] [space orientation="vertical" thickness="5"] **Solarus :** le cœur du moteur Solarus.<br/>
[icon-solarus icon="launcher" size="32"] [space orientation="vertical" thickness="5"] **Solarus Launcher :** l'application
permettant de sélectionner et de démarrer un jeu.

[/box]

### Comment jouer

Rien de plus simple ! [Téléchargez les quêtes](/fr/games) de votre choix puis
ouvrez-les depuis l'application Solarus Launcher.

[/column]

[column]

## Pour les créateurs de jeux

![Quest Editor](images/quest_editor_miniature.png)

[space thickness="20"]

Ce paquet est destiné à celles et ceux qui veulent **créer des quêtes Solarus**. Il ajoute au package des joueurs les outils nécessaires à la création de jeux.

[align type="center"]
[button-download type="primary" label="Télécharger" url="#" icon="download" icon-category="fa" icon-size="1x" category="quest-maker" modal-title="Télécharger" modal-button-label="Télécharger"]
[/align]

### Contenu du paquet

[box]

[icon-solarus icon="solarus" size="32"] [space orientation="vertical" thickness="5"] **Solarus :** le cœur du moteur Solarus.

[icon-solarus icon="launcher" size="32"] [space orientation="vertical" thickness="5"] **Solarus Launcher :** l'application
permettant de sélectionner et de lancer une quête.

[icon-solarus icon="editor" size="32"] [space orientation="vertical" thickness="5"] **Solarus Quest Editor :** l'éditeur de jeux.

[icon-solarus icon="quest" size="32"] [space orientation="vertical" thickness="5"] **Sample Quest :** un petit jeu avec quelques exemples.

[/box]

### Prise en main

Les tutoriels et la documentation vous guideront dans la création de votre quête Solarus.

* [Tutoriel Solarus](/fr/development/tutorials)
* [Documentation Solarus](https://www.solarus-games.org/doc/latest)

[/column]
[/row]

## Licence

[row]
[column width="3"]
[align type="center"]
![GPL v3 logo](images/gpl_v3_logo.png "GPL v3 logo")
[/align]
[/column]
[column]
Solarus est un **logiciel libre** ; vous pouvez le redistribuer et/ou le modifier sous les termes de la [GNU General Public License](https://www.gnu.org/licenses/gpl.html) telle que publiée par la Free Software Foundation, soit la version 3 de la licence, ou (selon votre choix) une version supérieure.
[/column]
[/row]
[space thickness="30"]
[row]
[column width="3"]
[align type="center"]
![CC logo](images/cc_logo.png "CC logo")
[/align]
[/column]
[column]
Solarus contient aussi des **ressources libres**, sous licence [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0). Vous êtes libres de les adapter et de les modifier, tant que vous les redistribuez sous la même licence et que vous créditez le(s) auteur(s) originels.
[/column]
[/row]

[space thickness="50"]

## Autres liens

Si vous cherchez autre chose :

* [Parcourir les autres releases Solarus](https://www.solarus-games.org/downloads/solarus/)
* [Comment compiler Solarus vous-même.](https://gitlab.com/solarus-games/solarus/blob/dev/compilation.md)
* [Le code source de tous les projets de la Solarus Team](https://gitlab.com/solarus-games)

[/container]
