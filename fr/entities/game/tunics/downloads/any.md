### Téléchargez le moteur

Pour jouer une quête Solarus, vous avez besoin de **Solarus Launcher** (ou, au moins, le **moteur**). Vérifiez d'abord s'il-vous-plait si vous l'avez [installé](/fr/solarus/download).

Si vous avez déjà Solarus Launcher, vous pouvez aller à l'**étape 2**.

### Téléchargez la quête

Cliquez sur le bouton suivant pour télécharger la quête sous forme de **fichier .solarus**. Vous pouvez l'ouvrir avec **Solarus Launcher** (ou depuis le terminal).
