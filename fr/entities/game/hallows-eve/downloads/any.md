### Télécharger Solarus

Pour ouvrir un jeu Solarus, vous avez besoin de l'application **Solarus Launcher**. Assurez-vous d'abord de l'avoir [installée](/fr/solarus/download).

Si vous avez déjà Solarus Launcher, vous pouvez passer à l'**étape 2**.

### Télécharger le jeu

Ce jeu n'est pas directement disponible sous forme de **fichier .solarus**. Cliquez sur le bouton suivant pour vous rendre sur la page du jeu sur **itch.io**.
