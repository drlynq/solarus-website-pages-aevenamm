### Halloween. Les années 90. Vous êtes un homme-citrouille.

À Halloween, la barrière entre le monde des humains et celui des esprits s'affaiblit. Les esprits jouent des tours, les morts peuvent être entraperçus, et les monstres peuvent errer parmi les humains.

Durant les années 90, incarnez Ichabod, un homme-citrouille qui tente sa chance pour pénétrer le monde des humains et essayer de louer une cassette de Jurassic Park au vidéo-club. Malheureusement, l'unique exemplaire a été emprunté par un adolescent qui jeté un sort de sorcière sur la ville.

Le gameplay est inspiré par les classiques, aussi bien anciens que modernes — un sens de l'exploration à la Zelda, avec le combat incisif et rapproché d'Hyper Light Drifter. Le tout baigné dans une atmosphère charmante en pixel-art, et un sens de l'humour stupide. La durée de jeu est d'environ 1 à 2 heures.

### Contrôles par défaut

| Touche   | Action |
|:--------:|--------|
| <kbd>←</kbd> <kbd>↑</kbd> <kbd>→</kbd> <kbd>↓</kbd> | Se déplacer |
| <kbd>Espace</kbd>                                    | Action / Esquiver |
| <kbd>C</kbd>                                        | Coup |
| <kbd>X</kbd>                                        | Attauqe spéciale |
| <kbd>V</kbd>                                        | Se soigner |
| <kbd>D</kbd>                                        | Pause / Carte |
| <kbd>D</kbd> (Dans le menu de pause)                | Sauvegarder et retourner à l'écran-titre |
| <kbd>F1</kbd>                                       | Schéma de contrôle |
| <kbd>F2</kbd>                                       | Options |
