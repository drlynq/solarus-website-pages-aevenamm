Max Mraz, qui a fait beaucoup parler de lui avec son précédent jeu [Yarntown](/fr/games/yarntown), travaillait aussi depuis longtemps sur un projet beaucoup plus ambitieux : **Ocean's Heart**. Le jeu est finalement sorti après plusieurs années de développement. Max, grâce à l'aide de la communauté Solarus et de son éditeur [Nordcurrent](https://www.nordcurrent.com/), est le premier auteur à publier un jeu commercial Solarus sur Steam.

Ocean's Heart est un action RPG épique vue de dessus en pixel art sublime. Vous y explorerez un bel archipel sous les traits de Tilia. Acceptez des contrats et éliminez des monstres, enfoncez-vous dans des donjons antiques et terrassez de terribles adversaires pour élucider le mystère d'Ocean's Heart. Le tout dans un pixel-art splendide et coloré.

- Voir le jeu sur [sa page dans la Bibliothèque de Quêtes Solarus](/fr/games/oceans-heart).
- Télécharger le jeu sur [Steam](https://store.steampowered.com/app/1393750/Oceans_Heart/)

[youtube id="jELtHA_VBJ0"]
