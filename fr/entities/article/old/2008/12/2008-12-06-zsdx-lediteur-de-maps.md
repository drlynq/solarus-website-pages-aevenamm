Bonsoir à tous :)

La création des maps avance bien. Depuis la dernière mise à jour, la plupart des maps du monde extérieur ont été dessinées, même s'il manque encore certains éléments de décors. La majorité de ce travail a été fait par Mymy ;).

Les images de ces maps arriveront dans une prochaine news, car celle-ci est consacré à quelque chose qui a attisé votre curiosité : l'éditeur de maps. Comme vous le savez sans doute, je développe pour ce projet un éditeur permettant de créer facilement les maps, graphiquement avec la souris, un peu comme avec RPG Maker, mais en plus adapté aux besoins spécifiques d'un Zelda comme celui-là.

L'éditeur permet de placer tous les objets d'une map : pas seulement les décors, mais aussi les objets interactifs comme les buissons, les coffres, les téléporteurs... et bientôt les ennemis.

Une rubrique dédiée à l'éditeur de maps vient d'être ouverte dans la section Zelda : Mystery of Solarus DX. Pour le moment, elle comporte entre autres une petite vidéo toute fraîche qui montre comme ça fonctionne ^_^.

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-editeur]Page consacrée à l'éditeur[/url][/li]
[li][url=http://www.zelda-solarus.com/zsdx/mapeditor.html]Vidéo de l'éditeur de maps[/url][/li]
[/list]