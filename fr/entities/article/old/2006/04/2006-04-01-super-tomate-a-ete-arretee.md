Super Tomate a été arrêtée par le GLOUPS (Groupe de Lutte Obstinée et Urgente contre les Plantes Solanacées). Les codes du site que j'ai donnés à Super Tomate nous ont en effet permis de la localiser lorsqu'elle les a utilisés pour prendre le contrôle du site.

Le site et le forum sont maintenant à nouveau rétablis à la normale. Les comptes de Super Tomate et de ses serviteurs légumes sont désormais bannis. Super Tomate se fait actuellement interroger par le GLOUPS, pour tenter de connaître ses réelles motivations.

Enfin, encore une bonne nouvelle : Couet a bien été libérée. Elle est saine et sauve bien que choquée (voir la photo ci-dessous). Une cellule psychologique a été mise en place afin de la remettre en état, pour qu'elle puisse continuer les mangas au plus vite !

[center][img]http://christopho.zelda-solarus.com/couet_lunettes.jpg[/img][/center]