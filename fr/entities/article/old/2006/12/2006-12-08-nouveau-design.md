On change tout !
Comme nous l'avions annoncé, voici le nouveau design tout beau tout neuf spécialement dédié à Twilight Princess qui sort aujourd'hui même :)

Saviez-vous que l'ancien design a eu de loin le record de longévité ? Il était en place depuis le 5 septembre 2004 ! Il était donc temps de le changer, et nous ne voulions pas manquer l'occasion de la sortie de Twilight Princess et de la Wii.

Ce nouveau design, sombre comme Twilight Princess, est un peu un retour aux sources puisqu'il est l'oeuvre de Metallizer. Il conserve les qualités du précédent au niveau de la navigation : un menu à gauche avec la liste des jeux, et un menu général en haut facilement accessible. Le design au fond sombre rend la lecture des pages plus confortable pour les yeux. La taille du texte a également été légèrement agrandie.

On a choisi d'avoir un design épuré afin d'avoir une interface sobre et sans fioritures. D'ailleurs, l'ensemble des images du design pèse au total moins de 57 Ko. Et pourtant c'est plutôt beau non ? Sur les deux côtés, vous aurez bien sûr reconnu le Link de Twilight Princess, si la résolution de votre écran est supérieure à 800*600. Plus votre résolution est importante, et plus vous pouvez en voir !

On pense que ce design vous surprendra et on espère qu'il vous plaira ! Il est prévu pour durer moins longtemps que le précédent. Le design qui le remplacera sera consacré à Zelda : Mercuris' Chest, mais on a encore le temps d'y penser ^_^