<p><img src="/images/link_mort.jpg" width="98" height="147" align="right"></p>

<p>Voici enfin les artworks officiels de Zelda Solarus (3 ans après tout de même !).</p>

<p>Ils illustrent le <a href="http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=notice">mode d'emploi</a> du jeu.</p>

<p>Vu qu?ils ne sont pas réalisés par Neovyze, « l?artworkeur » officiel de Mercuris? Chest, mais par moi, Gobbi, ils sont dans un style tout à fait différent, qui je l?espère vivement, vous plaira.</p>

<p><a href="http://www.zelda-solarus.com/jeux.php?jeu=zs&zone=notice">Mode d'emploi illustré de Zelda : Mystery of Solarus</a></p>