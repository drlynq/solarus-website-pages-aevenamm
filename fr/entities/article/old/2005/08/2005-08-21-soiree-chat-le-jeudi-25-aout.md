<p>Les différents sites membres du canal de discussion #Zelda, dont bien sûr Zelda Solarus, organisent une soirée spéciale le jeudi 25 août. Je vous laisse lire l'annonce officielle de cette soirée :</p>

<p><i>Oyé oyé ! Soyez tous présents le Jeudi 25 Août 2005 sur le T'Chat de #Zelda à partir de 21h (France) ou 15h (Québec) ! Cette soirée T'Chat est sponsorisée par tous les sites membres de #Zelda et a pour but de réunir le maximum de fans de Zelda, et bien sûr, redonner vie au canal #Zelda en y invitant peut-être des futurs habitués !<br>
Nous espérons que vous serez très nombreux à venir, sans quoi nous serions très déçus ^-^<br><br>

En premier lieu, il y aura un débat sur la saga de Zelda. Tous les domaines seront touchés, surtout la future sortie de Zelda : Twilight Princess.<br>
Bien sûr, tous les utilisateurs présents sur le chat pourront y participer ! Vous pourrez même échanger vos points de vue en public, ou en privé avec d'autres fans de Zelda.<br>
A partir de 22h/22h30 (selon la durée du débat), un quizz 100% Zelda sera lancé ! Bien sûr, tout est fait pour s'amuser et non pour gagner (n'espérez pas gagner des prix :P), mais c'est un très bon moyen pour se tester soi-même sur sa connaissance zeldaïque !<br>
Il n'y aura pas d'heure de fin de soirée, les utilisateurs pourront rester le temps qu'ils veulent (en espérant qu'ils deviennent par la suite des habitués ;)) !</i></p>

<p>Pour accéder au chat, le plus simple est de cliquer sur le lien "Chat" depuis notre site. Pour connaître plus de détails sur le fonctionnement du chat, je vous conseille de visiter le <a href="http://www.kidlogis.com/zeldachan" target="_blank">site officiel</a> du salon de discussion #Zelda. Soyez nombreux le 25 août !</p>