Nous sommes le 26 avril 2007 : il y a un anniversaire à fêter ! :D

Il y a 5 ans, le 26 avril 2002, est né Zelda : Mystery of Solarus. Ce jour a marqué un tournant dans l'histoire du site, puisque c'est depuis cette date que Zelda Solarus est un site consacré aussi aux Zelda officiels. En effet, avant cela , Zelda Solarus était un petit site qui se contentait de donner des nouvelles sur le développement du jeu.

J'imagine que peu d'entre vous étiez déjà visiteurs de Zelda Solarus il y a 5 ans. Depuis cette date, il s'est passé beaucoup de choses. Les visites ont beaucoup augmenté et nous sommes devenus l'un des sites Zelda francophones les plus connus, grâce à nos soluces, nos mangas et surtout notre création, Zelda : Mystery of Solarus. Vous avez été plus de 250 000 à télécharger le jeu depuis sa sortie.

Tout le monde ne sait pas que le projet Mystery of Solarus a été initié en 1995 avec une version entièrement sur papier... Si vous voulez en apprendre plus sur le déroulement du développement du jeu, visitez la page consacrée à l'historique de sa création. ;)

[list]
[li][url=http://www.zelda-solarus.com/jeux.php?jeu=zs&amp;zone=historique]Historique de la création du jeu[/url][/li]
[li][url=http://www.zelda-solarus.com/jeux.php?jeu=zs&amp;zone=versionpapier]Images de la version papier[/url][/li]
[/list]

Je terminerai en profitant de cet anniversaire pour remercier toute l'équipe qui a contribué au succès du site pendant ces 5 ans : Netgamer / Metallizer, @PyroNet, Couet, Geomaster, Seb le Grand, Gobbi, Noxneo et tous les autres. Merci pour tout ce que vous apportez ou avez apporté et longue vie à Zelda Solarus. Rendez-vous dans 5 ans car j'espère bien qu'on sera encore là ! ^_^