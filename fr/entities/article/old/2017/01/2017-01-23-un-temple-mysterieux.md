La capture d'écran de Zelda Mercuris Chest du jour est un temple majestueux dans lequel Link va devoir rechercher quelque chose de précieux

<a href="../data/fr/entities/article/old/2017/01/images/outside_crater_temple.png"><img class="aligncenter size-medium wp-image-38077" src="/images/outside_crater_temple-300x225.png" alt="outside_crater_temple" width="300" height="225" /></a>

On ne peut pas vous en dire plus, mais encore une fois c'est à Newlink que vous devez envoyer les félicitations pour ces graphismes.

&nbsp;