Salut à toutes et à tous !

<img class="aligncenter size-medium wp-image-38133" src="/images/zsxd2_title@2x-300x249.png" alt="zsxd2_title@2x" width="300" height="249" />

L<span class="operator">'</span>incroyable s<span class="operator">'</span>est produit !

Aujourd<span class="operator">'</span>hui, nous sommes en mesure de vous proposer, et ce bien avant la date annoncée de <span class="number">2023</span>, la version finale de notre nouveau Zelda : Mercuris Chess ! C<span class="operator">'</span>est non sans une certaine émotion et une certaine fierté que nous vous offrons cette création à télécharger dès maintenant.

<img class="aligncenter size-medium wp-image-38131" src="/images/zsxd2_box-300x214.jpg" alt="zsxd2_box" width="300" height="214" />
<ul>
	<li><strong><a href="http://www.zelda-solarus.com/downloads/zelda-xd2/win32/zelda-xd2-1.0.3-win32.zip">Télécharger Zelda Mercuris Chess</a>, le jeu complet</strong></li>
</ul>
Cette version finale du jeu est le fruit de longues nuits de développement, de mapping, de café et de Schoko-bons. Nous espérons que vous profiterez bien de cette nouvelle épopée de Link en <span class="number">2</span>D qui vous fera traverser des contrées merveilleuses, parcourir des donjons retords et évoquer quelques souvenirs.

<img class="aligncenter size-medium wp-image-38129" src="/images/artwork_link-300x258.png" alt="artwork_link" width="300" height="258" />

Beaucoup d'entre vous devaient se douter qu'une sortie était proche, étant donné le rythme des news depuis le début de l'année ! Dites-nous si c'était le cas !

<img class="aligncenter size-medium wp-image-38128" src="/images/artwork_grump-292x300.png" alt="artwork_grump" width="292" height="300" />

N<span class="operator">'</span>hésitez pas à commenter vos réactions sur les forums, nous avons hâte de découvrir votre ressenti sur cette nouvelle aventure qui clôt des années de préparation et de passion.

Pour finir je tiens à rendre hommage à toute l'équipe qui a travaillé d'arrache-pied sur ce projet complètement dingue. La Solarus Team a de quoi être fière, avec des gens qui ont énormément de talent et une motivation inépuisable ! Merci pour les souvenirs de Game Jam, bravo pour avoir mené ce projet à bien !

À bientôt et bon jeu !

&nbsp;