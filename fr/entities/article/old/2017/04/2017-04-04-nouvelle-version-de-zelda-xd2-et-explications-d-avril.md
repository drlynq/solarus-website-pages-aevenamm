&nbsp;

Pour celles et ceux qui n'ont pas tout suivi ou n'ont pas encore cliqué sur le lien, la news postée le 1er avril à 0h00 peut mériter quelques explications.

Il s'agit bien d'un jeu complet, intitulé officiellement <strong>Zelda XD2 : Mercuris Chess</strong>. Attention, avec "Ches<strong>s</strong>" et pas "Ches<strong>t</strong>" !!! Aoutch !

Rien à voir avec Zelda Mercuris Chest dont le développement se poursuit sereinement avec une capture d'écran publiée chaque semaine.

Zelda XD2 : Mercuris Chess est un projet différent. Développé secrètement par la Solarus Team depuis fin janvier seulement, il s'agit ni plus ni moins de la suite directe de <strong>Zelda Mystery of Solarus XD</strong> notre jeu parodique, sorti lui aussi un 1er avril : c'était en 2011.

Cette suite est donc bien sûr un jeu à vocation humouristique dans la lignée de Zelda Mystery of Solarus XD. Mais vous verrez vite que c'est encore plus travaillé et abouti, le moteur Solarus 1.5 d'aujourd'hui étant bien plus puissant et plus stable qu'en 2011, et l'équipe étant plus nombreuse avec notamment Neovyse qui a passé des dizaines d'heures à faire des graphismes inédits assez incroyables. Metallizer et moi avons réalisé les donjons. Renkineko, BenObiWan, Valoo, Diarandor et Mymy ont également créé quelques maps ou scripts et ont apporté plein de bonnes idées.

<img class="aligncenter size-medium wp-image-38144" src="/images/blob-300x225.png" alt="blob" width="300" height="225" />

Je ne vais pas vous parler du scénario ici, je montre juste cette petite capture d'écran qui ne dévoile rien de trop important :) Vous découvrirez en jouant !

Sachez qu'il n'est pas nécessaire d'avoir joué à Zelda Mystery of Solarus XD pour jouer à Zelda XD2. Mais si vous y avez joué, vous apprécierez les nombreuses références.

Quant à la durée de vie, elle devrait être assez similaire à Zelda Mystery of Solarus XD (environ 5 heures pour les initiés), sans doute un peu plus longue si vous voulez finir le jeu à 100%.

On a tout donné pour finir le jeu dans les délais. On s'est aussi bien amusé à réaliser tout ça, alors on espère une seule chose : que vous allez bien vous marrer en jouant !
<ul>
	<li><strong><a href="http://www.zelda-solarus.com/downloads/zelda-xd2/win32/zelda-xd2-1.0.3-win32.zip">Télécharger Zelda Mercuris Chess</a> 1.0.3</strong> : nouvelle version avec de nombreuses petites corrections et améliorations. Vos sauvegardes restent compatibles.</li>
</ul>
&nbsp;