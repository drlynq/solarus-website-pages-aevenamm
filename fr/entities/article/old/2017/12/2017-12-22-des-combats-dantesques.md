Salut !

Vous souvenez-vous des boss que vous aviez affrontés dans<strong> Mystery of Solarus DX </strong>?

Il faut reconnaître que Christopho avait réussi son pari d'enfin proposer des monstres très variés et animés pour ses différents donjons avec une bonne dose de challenge.

Avec <strong>Mercuris' Chest</strong>, nous allons essayer de réitérer cet exploit voire plus, avec des boss offrant des combats épiques et dantesques même si rien n'est encore gravé dans le marbre.

Aujourdhui, nous vous proposons avant les fêtes de fin d'années un concept art de l'un d'entre eux, en vous souhaitant de la part de toute l'équipe un joyeux Noël par avance. ;)

<a href="/images/MCconceptartboss.png"><img class="alignnone size-full wp-image-38233" src="/images/MCconceptartboss.png" alt="" width="296" height="245" /></a>

&nbsp;

&nbsp;