Hier soir a été officiellement posée la première pierre de la principale ville de Mercuris' Chest !

Au cours d'une soirée de <a href="https://www.livecoding.tv/video/zelda-mercuris-chest-mapping-avec-newlink/">mapping en live-streaming</a> en compagnie de Newlink, nous avons en effet inauguré la construction d'une immense et prospère ville.

<a href="../data/fr/entities/article/old/2015/07/images/town.png"><img class=" size-medium wp-image-37928 aligncenter" src="/images/town-300x225.png" alt="town" width="300" height="225" /></a>

Un grand bravo à Newlink qui a réalisé énormément de magnifiques graphismes inédits, comme ceux que vous voyez sur cette capture d'écran. Nous allons pouvoir les utiliser pour cette ville mais aussi pour l'ensemble du jeu.

À bientôt pour d'autres infos !