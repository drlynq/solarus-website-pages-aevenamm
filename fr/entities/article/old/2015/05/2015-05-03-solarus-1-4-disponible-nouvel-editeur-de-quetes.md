UPDATE : un correctif vient d'être publié, il s'agit de la version <del>1.4.1</del> 1.4.2 qui corrige plusieurs bugs dont des crashs. Il est donc conseillé de retélécharger Solarus si vous avez la version 1.4.0 !

La création de jeux avec Solarus vient de franchir un nouveau palier. Je vous le promettais depuis longtemps dans les tutoriels : tout beau tout neuf, le nouvel éditeur de quêtes Solarus est maintenant disponible !
<h3>Solarus Quest Editor entièrement refait</h3>
Nous avons travaillé dur (et parfois jour et nuit si vous avez suivi les sessions de live-coding) pour totalement recréer l'éditeur de quêtes. L'ancien est maintenant à la poubelle, le nouveau est plus beau, plus rapide, plus intuitif, mais surtout, il est désormais en Français !

Si vous souhaitez utiliser Solarus pour développer votre propre jeu, vous verrez qu'il y a de nombreuses nouveautés. Les morceaux qui manquaient sont désormais disponibles (merci Maxs !) : un éditeur de dialogues, un éditeur de chaînes de caractères et un éditeur de propriétés de la quêtes.

<a href="../data/fr/entities/article/old/2015/05/images/tileset-editor.png"><img class="aligncenter" src="/images/tileset-editor-300x181.png" alt="" width="300" height="181" /></a>

Télécharger <a href="http://www.solarus-games.org/downloads/solarus/win32/solarus-1.4.2-win32.zip">Solarus 1.4 + l'éditeur de quêtes</a> pour Windows
<ul>
	<li>Télécharger le <a href="http://www.solarus-games.org/downloads/solarus/solarus-1.4.2-src.tar.gz">code source</a></li>
	<li>Liste complète des <a href="https://github.com/christopho/solarus/blob/v1.4.0/ChangeLog">changements</a></li>
	<li><a href="http://www.solarus-games.org/">Blog de développement Solarus</a></li>
	<li>Comment <a href="https://www.youtube.com/watch?v=St8Z2dtH7OA">convertir votre quête</a> de Solarus 1.3 vers Solarus 1.4</li>
	<li><a href="https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufySXw9_E-hJzmzSh-PYCyG2">Tutoriels vidéo</a></li>
</ul>
Le nouvel éditeur est encore expérimental, plusieurs bugs vont être corrigés dans les tout prochains jours. N'hésitez pas à en signaler si vous en trouvez !
<h3>Et maintenant ?</h3>
Après quelques mois passés à travailler sur cet éditeur, le prochain objectif est maintenant de continuer le projet Mercuris' Chest, tout en poursuivant les tutoriels car vous êtes nombreux à vous intéresser à la création de jeux. Il se peut que je diffuse des sessions de live-streaming de la création de Mercuris' Chest