<p>Une des grandes innovations de Zelda : Mystery of Solarus : le nouveau menu. Pour commencer, voici deux screenshots :</p>

<table border="0" cellpadding="15" cellspacing="0" align="center"> <tr align="center"> <td><a href="/images/solarus-ecran16.png" target="_blank"><img src="/images/solarus-ecran16.png" width="150" border="0"></a></td> <td><a href="/images/solarus-ecran23.png" target="_blank"><img src="/images/solarus-ecran23.png" width="150" border="0"></a></td> </tr> </table>

<p>Comme vous pouvez le voir, le menu est inspiré de Zelda 3. Zelda : Mystery of Solarus est (à ma connaissance) le premier jeu à ne pas utiliser le menu texte par défaut de RPG Maker 2000 (je parle bien sûr du jeu final et non pas de la <a href="http://www.zelda-solarus.com/demo.php3">démo</a>).</p>

<p>Lorsque vous appuyez sur la touche Echap, le menu graphique révolutionnaire apparaît. Ensuite, vous contrôlez un curseur pour sélectionner "Retour au jeu", "Sauvegarder", et "Quitter". Vous pouvez aussi choisir un objet, de la même manière que dans A Link to the Past. La principale différence est que l'objet que vous sélectionnez est utilisé directement lorsque vous appuyez sur Espace. Par exemple, lorsque vous sélectionnez les Bottes de Pégase et que vous appuyez sur Espace, le menu disparaît et Link se met à foncer comme un dingue. Ensuite, vous pouvez réutiliser les Bottes de Pégase ou utiliser un autre objet en revenant au menu.</p>

<p>Dans le menu, sont également affichés vos Coeurs, vos Rubis, vos Fragments de Coeur, votre Equipement, le nombre d'enfants sauvés, les objets de donjon si nécessaire, et même le temps de jeu !</p>

<p>Voilà, vous savez tout...</p>