<p>Salut à tous !</p>

<p>Le quatrième donjon est quasiment terminé ! Il ne manque plus que le Boss, la Carte et la Boussole. Le donjon est assez court, mais possède quand même quelques énigmes intéressantes...</p>

<p>Voici trois nouveaux screenshots :</p>

<table border="0" cellpadding="15" cellspacing="0" align="center">
<tr align="center">
<td><a href="/images/solarus-ecran37.png" target="_blank"><img src="/images/solarus-ecran37.png" width="150" border="0"></a></td>
<td><a href="/images/solarus-ecran38.png" target="_blank"><img src="/images/solarus-ecran38.png" width="150" border="0"></a></td>
<td><a href="/images/solarus-ecran39.png" target="_blank"><img src="/images/solarus-ecran39.png" width="150" border="0"></a></td>
</tr>
</table>

<p>Vous pouvez retrouver ces screenshots et les 34 autres dans la <a href="http://www.zelda-solarus.com/galerie.php3">galerie&nbsp;d'images</a>.</p>

<p>Echangez vos impressions sur le <a href="http://www.zelda-solarus.com/forum.php3">forum</a> !</p>