<p>Salut à tous !</p>

<p>Le sixième donjon est bouclé depuis quelques jours déjà. C'est la première fois que ça va si vite ! Mais rassurez-vous : la difficulté est bien au rendez-vous. En effet, ce niveau 6 est encore plus compliqué que tous les autres, que ce soit du point de vue des énigmes que du point de vue des combats. Mais je ne vous en dirait pas plus...</p>

<p>Je suis en ce moment en train de programmer de nouvelles grottes en vue du septième donjon. Quant à Netgamer, il s'occupe actuellement de faire la carte et la boussole du donjon 5.</p>

<p>Voilà pour ces quelques nouvelles... A bientôt :)</p>