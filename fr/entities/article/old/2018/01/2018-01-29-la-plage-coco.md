Cette semaine, pour le projet A Link to the Dream, je vous présente la<strong> plage Coco</strong> qui se trouve au sud du village des Mouettes, pas loin du donjon 1: la cave Flagello. Sachez que cette partie de la carte <strong>est presque terminée</strong>.

<a href="../data/fr/entities/article/old/2018/01/images/1.png"><img class="alignnone size-medium wp-image-38263" src="/images/1-300x240.png" alt="" width="300" height="240" /></a> <a href="../data/fr/entities/article/old/2018/01/images/2.png"><img class="alignnone size-medium wp-image-38264" src="/images/2-300x240.png" alt="" width="300" height="240" /></a>

Comme d'habitude, n'hésitez pas à donner votre avis sur ces deux images. En tout cas, celle de gauche fait particulièrement rêver !