Grâce à une grosse contribution de Vlag, la prochaine version de Solarus permettra de réaliser toutes sortes d'effets graphiques. Les shaders OpenGL seront en effets supportés dans Solarus 1.6.

Jusqu'à présent, les jeux Solarus pouvaient proposer certains filtres de lissage prédéfinis, afin de rendre l'image moins pixellisée lorsque la fenêtre est agrandie ou en plein écran. Mais bientôt, chaque jeu pourra proposer ses propres shaders et modifier le rendu comme il le souhaite, le tout de façon plus performante. Les shaders peuvent toujours être utilisés pour lisser l'image, ou alors pour réaliser d'autres effets plus ou moins originaux. La plupart des émulateurs proposent d'ailleurs un large choix de shaders pour modifier le rendu graphique.

Dans Zelda Mercuris' Chest, on n'est pas encore certains de quels shaders seront retenus dans le jeu final, mais voici quelques premières expériences :

<a href="../data/fr/entities/article/old/2018/01/images/shader_6xbrz.png"><img class="aligncenter size-medium wp-image-38247" src="/images/shader_6xbrz-300x225.png" alt="" width="300" height="225" /></a>

<a href="../data/fr/entities/article/old/2018/01/images/shaders.png"><img class="aligncenter size-medium wp-image-38248" src="/images/shaders-300x225.png" alt="" width="300" height="225" /></a>

Il est même possible de faire un effet très semblable au célèbre Mode 7 de la Super Nintendo mais je vous réserve ça pour une prochaine news !