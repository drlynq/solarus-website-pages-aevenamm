Cette semaine, nous avons bien travaillé sur les personnages du jeu A Link to the Dream et sur la<strong> forêt enchantée</strong>. Cette dernière est vraiment aboutie à présent !

Voici quelques images de ce lieu très important dans le début du jeu.

<a href="../data/fr/entities/article/old/2018/02/images/4-1.png"><img class="alignnone size-medium wp-image-38284" src="/images/4-1-300x240.png" alt="" width="300" height="240" /></a> <a href="../data/fr/entities/article/old/2018/02/images/3-1.png"><img class="alignnone size-medium wp-image-38283" src="/images/3-1-300x240.png" alt="" width="300" height="240" /></a> <a href="../data/fr/entities/article/old/2018/02/images/2-1.png"><img class="alignnone size-medium wp-image-38282" src="/images/2-1-300x240.png" alt="" width="300" height="240" /></a> <a href="../data/fr/entities/article/old/2018/02/images/1-1.png"><img class="alignnone size-medium wp-image-38281" src="/images/1-1-300x240.png" alt="" width="300" height="240" /></a>

Je suis certain que vous n'aurez aucun mal à reconnaitre les différents endroits qui sont représentés sur ces images.