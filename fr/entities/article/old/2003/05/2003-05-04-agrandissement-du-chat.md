<p>Il faut bien le reconnaître, notre chat IRC (dialogue en direct) était toujours désert ou presque jusqu'à maintenant. Pour y remédier, nous avons fusionné avec le canal #zelda, que nous partageons désormais avec deux autres grands sites sur Zelda : <a href="http://www.puissance-zelda.com" target="_blank">Puissance-Zelda</a> et <a href="http://www.zelda-boss.fr.st" target="_blank">Zelda-Boss</a>. Nous faisons maintenant partie du plus grand chat francophone sur Zelda !</p>

<p>Bien sûr, nous continuerons à faire des soirées chat, y compris sur le thème de Zelda : Advanced Project, l'avantage est que maintenant il y aura du monde !</p>

<p><a href="http://connexion.asterochat.com/?id=23857" target="_blank">Accéder au chat</a></p>