<p>Je viens juste de rentrer de vacances et j'en ai donc profité pour rouvrir le site le plus vite possible. Une personne du site qui hébergeait nos images et nos téléchargements s'était amusée à tout supprimer, donc forcément plus rien ne marchait.</p>

<p>Mais c'est maintenant réparé. Tout est censé fonctionner à nouveau, y compris le téléchargement de la démo de Zelda : Advanced Project.</p>

<p>Encore toutes nos excuses pour cet incident qui souhaitons-le ne se reproduira plus...</p>