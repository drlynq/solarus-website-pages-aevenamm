Une petite news simplement pour vous remercier d'être aussi passionnés par nos jeux. [url=http://www.zelda-solarus.com/jeu-zsdx-download]Zelda Mystery of Solarus DX[/url] a en effet dépassé il y a quelques jours la barre des 100 000 téléchargements !

[center][img]http://www.zelda-solarus.com/images/zsdx/dungeon_8_holes.png[/img][/center]

Si nos créations vous plaisent, continuez à en parler autour de vous. ^_^

Encore merci à tous. L'épopée continue?