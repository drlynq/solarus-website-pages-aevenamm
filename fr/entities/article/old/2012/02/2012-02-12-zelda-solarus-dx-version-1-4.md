[b]Mise à jour : [/b] une version 1.4.1 est disponible, elle corrige un [b]bug critique de la version 1.4[/b] qui nous avait échappé et qui pouvait vous bloquer dans le donjon 5. Vous devez donc retélécharger le jeu en version 1.4.1 si vous ne voulez pas avoir de problèmes. Votre sauvegarde sera bien sûr conservée.

[b]Article original : [/b] Nous venons de publier une nouvelle version de [url=http://www.zelda-solarus.com/jeu-zsdx-download]Zelda Mystery of Solarus DX[/url]. Il s'agit de la version 1.4. Parmi les principaux changements :
[list]
[li]On peut maintenant courir directement avec la touche Action (il n'est plus nécessaire d'équiper les Bottes de Pégase)[/li]
[li]Correction d'un bug qui faisait dépenser plusieurs petites clés sur la même serrure[/li]
[li]Plusieurs optimisations importantes[/li]
[li]Nouvel écran de fin[/li]
[li]Modification de l'agencement de quelques salles[/li]
[li]Nombreuses améliorations diverses[/li]
[li]Correction de bugs mineurs[/li]
[/list]

Cette mise à jour comporte donc une correction de bug important, une nouveauté dans le gameplay et surtout de nombreuses améliorations. Plusieurs énigmes ont été réagencées car elles étaient trop fastidieuses ou sans intérêt. Il y a aussi des énigmes un peu longues qui sont désormais sauvegardées (alors qu'elles ne l'étaient pas, ce qui veut dire que vous étiez obligés de les refaire à chaque passage). Il y a même une salle qui a été entièrement refaite dans le donjon 4. Rien de révolutionnaire donc, mais du confort en plus et moins de prises de tête inutiles.

Je vous conseille donc de retélécharger et de réinstaller le jeu. Votre sauvegarde reste bien sûr valable et sera disponible dans le menu, vous n'avez rien de spécial à faire pour cela.

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-download]Télécharger Zelda Mystery of Solarus DX[/url][/li]
[/list]

Nous travaillons actuellement à traduire le jeu en anglais et en allemand. C'est un travail particulièrement long étant donné la quantité de dialogues du jeu. Dans une prochaine version, ces langues seront donc disponibles :). Et si vous souhaitez contribuer à une nouvelle traduction, n'hésitez pas à [url=http://www.zelda-solarus.com/contact.php]me contacter[/url] 