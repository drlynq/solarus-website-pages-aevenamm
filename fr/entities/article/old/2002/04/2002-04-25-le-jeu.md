<p>Ca y est !</p>

<p>Ca y est ! Et cette fois, pas de faux départ !</p>

<p>Je pense que vous devez êtes surpris de ce nouveau design, même si vous vous y attendiez peut-être ! Comme vous pouvez le constater le site s'agrandit, puisqu'en plus de Zelda Solarus, il va aussi parler de tous les autres Zelda, les vrais ! Avec des soluces complètes, des images, des musiques... Laissez-nous juste le temps de les faire !</p>

<p>Vous pouvez lire l'<a href="http://www.zelda-solarus.com/site.php3?page=edito">édito</a> de Netgamer, qui vous présentera le jeu et le site. Autre chose à ne pas manquer : la soluce complète de <a href="http://www.zelda-solarus.com/jeux.php3?jeu=z6&zone=soluce">Zelda : Majora's Mask</a> est déjà en ligne, même si elle ne va pas encore jusqu'à la fin du jeu). D'autres pages sont également créées, vous pouvez parcourir le menu de gauche. Le site se remplira au fur et à mesure avec le temps.</p>

<p>Mais venons-en à ce que vous attendez depuis des mois, c'est-à-dire <a href="http://www.zelda-solarus.com/jeux.php3?jeu=zs&zone=download">Zelda : Mystery of Solarus</a>, le jeu, en version complète, qui est disponible !</p>

<p>Et puis pour finir, n'oubliez pas de visiter les nouveaux forums, beaucoup plus performant que l'ancien (qui avait cependant le mérite d'être programmé par mes soins...). Retenez cette adresse : <a href="http://www.zsforums.fr.st" target="_blank">http://www.zsforums.fr.st</a>. Trois forums sont installés : un forum rien que pour ZS, à consulter si vous êtes bloqué ou si vous voulez tout simplement parler du jeu, un forum sur tous les Zelda et enfin un forum libre.</p>

<p>Voilà, je crois que tout est dit. Je vous souhaite une bonne quête. Si, si, c'est sincère.</p>