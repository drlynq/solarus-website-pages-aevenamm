<p>Salut à tous !!</p>

<p>Netgamer m'a donné de ses nouvelles ! Après deux semaines d'absence sur le web (il ne souhaite pas que je raconte sa vie mais la cause de cette absence commence par un V, bref voilà... mais parlons plutôt de Zelda : Mystery of Solarus), il s'est enfin manifesté et nous avons longuement discuté de l'avenir du jeu.</p>

<p>Nous pouvons d'ores et déjà vous dire que la date de sortie se précise, et que nous vous la communiquerons très bientôt ! Sachez qu'il nous reste encore beaucoup de boulot mais que nous travaillons dur ! Une fois les deux derniers donjons programmés, il restera encore de nombreuses améliorations techniques à apporter, avant de passer à la longue période de tests.</p>