<p>Salut à tous !</p>

<p>Ca faisait longtemps que vous nous le demandiez et que nous vous le promettions, alors voilà qui devrait vous faire plaisir : nous avons décidé de publier sur le site une nouvelle image de la démo de Zelda Solarus 2 (ZAP) chaque jour !</p>

<p>Un nouveau screenshot est donc d'ores et déjà disponible dans la <a href="http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=scr">galerie d'images</a> de la démo.</p>

<p>Nous allons donc mettre en ligne une nouvelle image de notre création tous les jours pendant environ un mois. Vous aurez donc de quoi vous faire patienter avant la démo ;-)</p>

<p>Accéder à la rubrique de <a href="http://www.zelda-solarus.com/jeux.php?jeu=zf">Z.A.P.</a></p>