Nous avons profité de l'Assemblée Générale annuelle de Solarus Labs pour faire un retour sur ce qui s'est passé en 2021, et quels sont les projets à venir pour Solarus en 2022.

## Projets finis en 2021

La mise à jour de correction v1.6.5 a été publiée en 2021. Elle contient des correctifs attendus depuis très longtemps pour le moteur, l'éditeur de quêtes et les quêtes elles-mêmes. La demande de correction de bug la plus demandée de ces dernières années a finalement été répondue : vous pouvez désormais redimensionner vos *tiles* sans l'horripilant bug de la touche R.

![Solarus 1.6.5 release](images/release-1-6-5.jpeg)

Les forums se sont vus offrir un nouveau look, pour s'aligner sur les couleurs officielles de Solarus.

![New forums theme](images/new-forums.png)

Le premier jeu commercial fait avec Solarus a été publié sur Steam et les plateformes similaires : *Ocean's Heart*. Il a été réalisé par Max Mraz presque tout seul, avec un peu d'aide de Llamazing et d'autres contributeurs Solarus.

![OH on Switch](images/oh-thumbnail.jpeg)

Et maintenant, place au secret que nous avons gardé si longtemps...

*Ocean's Heart* est aussi le premier jeu Solarus a être publié sur Nintendo Switch, car le moteur a été porté sur la fameuse console portable par Grégoire pendant l'année 2021. La sortie Switch d'*Ocean's Heart* fut début 2022, bien que le portage a été fait l'année dernière.

![OH on Switch](images/oh-switch.png)

Grâce au buzz généré par *Ocean's Heart*, Solarus a reçu de nombreux nouveaux arrivants ! Il a même été utilisé pour enseigner la programmation dans un cours avec 2500 étudiants.

## Projets en cours

Christopho travaille d'arrache-pied pour publier un nouveau tutoriel vidéo chaque semaine afin d'expliquer comment utiliser Solarus. Il y en a 45 à l'heure où ces lignes ont été écrites.

![Video tutorials](images/video-tutorials.png)

*A Link to the Dream*, la refonte de *Link's Awakening* par Binbin, progresse également. Puisqu'il contient de nombreuses et complexes interactions, c'est un bon moyen de pousser le moteur dans ses limites, d'ajouter des nouvelles fonctionnalités ou de corriger des bugs.

![A Link to the Dream](images/alttd-teaser.jpeg)

Olivier travaille sur une nouvelle version de *Solarus Launcher* qui a bien avancé. Elle devrait être incluse avec la prochaine version de Solarus.

![New launcher](images/new-launcher.png)

Olivier a aussi travailé sur un thème Qt sombre pour *Solarus Quest Editor*.

Hugo travaillait sur un nouveau site web de documentation pour tous les projets Solarus, mais a été stoppé en 2021 pour cause de déménagement. Ce nouveau site contiendra la doc de l'API Lua, la doc du code source C++, les tutoriels et les manuels utilisateurs. Il utilisera Material for MkDocs.

![MkDocs](images/mkdocs.png)

Grégoire a porté Solarus to WASM, ce qui permet désormais de l'utiliser directement dans le navigateur web. Cela fonctionne très bien, et nous espérons pouvoir l'inclure avec la prochaine version de Solarus.

Grégoire a aussi travaillé sur le portage Android. Il fonctionne et est en phase de beta-test.

## De l'innattendu 2021

Un incendie a détruit le bâtiment d'OVH, et le serveur de Solarus a été détruit. Les pièces jointes du forum ont été perdues, mais tout le reste a pu être sauvé !

![OVH Fire](images/ovh-fire.png)

## Planned for 2022

- Solarus ~~1.7~~ **2.0** :
  - Fonctionnalités multi-joueurs (mergé dans la branche `dev` !).
  - Un tout nouveau *Solarus Launcher*.
  - L'application Android (beta-test).
  - Thème sombre pour *Solarus Quest Editor*.
  - Nouveau logo.
  - Rafraîchissement du site web.

- Développement de jeux :
  - *Children of Solarus* (priorité numéro un).
  - *The Legend of Zelda: Oni-Link Begins*.
  - *The Legend of Zelda: Mercuris' Chest* (La fameuse blague sur 2023 devient de moins en moins drôle...).
  - *The Legend of Zelda: A Link to the Dream* (Le bébé de Binbin).

## Conclusion

Malgré l'épidémie de covid et beaucoup de mauvais évènements innatendus, 2021 a été une excellente année pour Solarus, et annonce un 2022 prometteur ! Merci à tous les membres de la communauté qui rendent ce projet possible.
