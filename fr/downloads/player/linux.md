Solarus pour Linux est officiellement disponible via le **Snap Store**:

[![Snapstore](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/solarus)

Alternativement, vous pouvez cliquer sur le bouton suivant pour télécharger le **paquet Snap** directement.
